# Scala 3

## General
Familiarity with popular ML libraries such as TensorFlow, PyTorch, and Huggingface


Golden instruction:


https://medium.com/scala-3


https://deanwampler.github.io/scala3-highlights.html



### launch scala3 REPL:

https://github.com/lampepfl/dotty-feature-requests/issues/131


```
scala

scala -source:future -explain

scala> :load test.scala

^D

```

- https://www.baeldung.com/scala/dotty-scala-3


- regarding -source vararg values, check.  https://docs.scala-lang.org/scala3/reference/language-versions/source-compatibility.html




## New Features

### AnyKind, Kind polymorhism

https://www.scala-lang.org/api/current/scala/AnyKind.html

https://docs.scala-lang.org/scala3/reference/other-new-features/kind-polymorphism.html


AnyKind 

- is a supertype of all other types no matter what their kind is.

- It is also assumed to be kind-compatible with all other types. 

- is treated as a higher-kinded type (so it cannot be used as a type of values), but at the same time it has no type parameters (so it cannot be instantiated).

### Universal Apply()

Companion obj apply() now taken care by compilor, no `new` way anymore

### Export Clause

https://docs.scala-lang.org/scala3/reference/other-new-features/export.html

Export targets object, not class or trait

#### ?? Given instance handling in exporting

#### Why needed

object-oriented languages including Scala made it much easier to use inheritance than composition. Howver, Principle of decoupling prefers composition as it restrict overriding scope

Also this pattern can take role of V2 package object for utils defined and used within scope

#### extend, then create instance, then export sample

```
class StringOps(x: String):
  def *(n: Int): String = ...
  def capitalize: String = ...

extension (x: String)
  def take(n: Int): String = x.substring(0, n)
  def drop(n: Int): String = x.substring(n)
  private def moreOps = new StringOps(x)
  export moreOps.*

```


##### Composite vs Decorator



Composite gives an unified interface to a leaf and composite.

Decorator decorator gives additional feature to leaf, while giving unified interface.

From Go4: ```A decorator can be viewed as a degenerate composite with only one component. However, a decorator adds additional responsibilities—it isn't intended for object aggregation```

### Opaque Type Alias

https://docs.scala-lang.org/scala3/reference/other-new-features/opaques-details.html


https://docs.scala-lang.org/scala3/reference/other-new-features/opaques.html

`extension (x: SomeType) {def xx }` ways to define functional extension


`type T >: L <: U`, in general L is Nothing , U is Any (or AnyVal if you insist)




#### Why opaque type

The main reason for using the opaque type alias is to model the domain entities more safely. We might need to provide some restrictions to the values as per the domain.

https://www.baeldung.com/scala/opaque-type-alias#:~:text=The%20main%20reason%20for%20using,and%20build%20the%20objects%20safely.

Scala 2 has value classes


### open, final, sealed

open (new for 3.0) only make sense to class def, and needs to be explicitly decleared. `trait`` & `abstract class``

`final val `, it is for class attribute, preventing being overriden in subclass

### Inline soft keyword

https://docs.scala-lang.org/scala3/guides/macros/inline.html

`inline` means defined function/val will be put in called place at compile time rather than just a reference to somewhere else in the memory

## Reading misc

### `For` comprehension

https://docs.scala-lang.org/tour/for-comprehensions.html


### syntax summary

https://docs.scala-lang.org/scala3/reference/syntax.html

###  3 things focus on scala:  Type, object, function


### scala3(dot compiler) focuse on [Dependent Object Types]

 
- compound type: intersection (A & B), union (A | B)

- to replace 'with' compound
https://docs.scala-lang.org/scala3/reference/new-types/type-lambdas.html#:~:text=A%20type%20lambda%20lets%20one,carry%20%2B%20or%20%2D%20variance%20annotations.

- functional extension

```
extension (c: C)
  def xxx
```

- trait can have args


https://docs.scala-lang.org/scala3/reference/other-new-features/trait-parameters.html

However, conflict will be introduced, scenarios like:

```
trait Person(val name: String){
	def getMe() : String = name 
}

// trait Adult(val name: String) extends Person { // compile fail
trait Adult extends Person {
	def smokeOrDrink(): Unit={}
}

// class Employee extends Adult("John"), Person("Bill") //too many arguments for constructor Adult in trait Adult: (): Adul

// class Employee extends Adult("Jack"), Person  //missing argument for parameter name of constructor Person in trait Person

class Employee extends Adult, Person("Bill")
```

- trait with context parameters
Abount context parameter: https://docs.scala-lang.org/scala3/book/ca-context-parameters.html#:~:text=Context%20Parameters%20allow%20you%20to,fill%20in%20the%20missing%20arguments.



- enum
```
enum XXX:
    case A, B, C
```

- Implicits COMPLETELY - refactory !!! (given / using )

https://docs.scala-lang.org/scala3/book/ca-context-parameters.html#:~:text=Context%20Parameters%20allow%20you%20to,fill%20in%20the%20missing%20arguments.


`def renderWebsite(path: String)(using Config): String{ .. }` in scala3, using only follow with type OK  

- summon defined in predef.

`def summon[T](using x: T): x.type = x`


- Where implicits being found

https://docs.scala-lang.org/tutorials/FAQ/index.html#where-does-scala-look-for-implicits

- contextual env now like:

```
vvvvv
given scala.concurrent.ExecutionContext = 
  ExecutionContext.global

import scala.concurrent.Future
def square(i: Int)(using ec: ExecutionContext): Future[Int] = {
                    ^^^^
  Future(i * i) 
}
```

- Regarding import (given part is not included in import *)

```
object A:
  class TC
  given tc: TC = ???

...
object B:
  import A.{given, *}


```



- Trait impl

```
given $trait with
    override doX() = ...
```

- type conversion
```
given scala.language.implicitConversions.Conversion[String, Int] = ...
```



- Extension method (2 vs 3)

v3:
```
extension(i: T) def bla = someFunc(i)
^^^^^^^^
//a DSL define in compiler
```

- Type lambda (I call that "curlied type", thinking about curley function/partial applied function)

```
type MapKV[K] = [V] =>> Map[K,V]
type MapKV = [K] =>> [V] =>> Map[K,V]

```


Type lambda vs polymorphic function (curley function with type definition being added to head):


```
scala> val toMapF1g = [K,V] => (key: K) => (value: V) => Map(key -> value) // good poly morphic function

val toMapF1b = [K] => [V] => (key: K) => (value: V) => Map(key -> value) // bad poly morphic function
```

Dependent Function Type : method signature included type, which is type definition in class, is decided in running time.


### infix vs inline (Open re-research)


### Type class derivation (Not get point, and scala compiler not working)


### Scala macros (based on Abstract Syntax Tree )

https://docs.scala-lang.org/scala3/guides/macros/reflection.html

## 


https://blog.rockthejvm.com/scala-3-type-lambdas/


https://docs.scala-lang.org/scala3/book/scala-features.html
Type class


https://medium.com/scala-3/scala-3-type-lambdas-polymorphic-function-types-and-dependent-function-types-2a6eabef896d


https://blog.rockthejvm.com/scala-3-type-lambdas/


https://www.baeldung.com/scala/category/scala-type-system


# Scala 2

- Primary constructor like `class A(val s: String) {..}` all body part will be executed


- case class apply/unapply/unapplySeq(Class)(unapplied implemented in its companion object)

https://www.baeldung.com/scala/extractor-objects


- String interpolation f"" (String.format()), compare to s(variable interpretation), r(raw for \n turn to return)




- generator pattern, or call syntax sugar of map/flatMap (functor/monad) (No discuss Monoid here)

for(<-) yield {
    
}

From Book Programming in Scala

```
For each iteration of your for loop, yield generates a value which will be remembered. It's like the for loop has a buffer you can’t see, and for each iteration of your for loop another item is added to that buffer. When your for loop finishes running, it will return this collection of all the yielded values. The type of the collection that is returned is the same type that you were iterating over, so a Map yields a Map, a List yields a List, and so on.

Also, note that the initial collection is not changed; the for/yield construct creates a new collection according to the algorithm you specify.
```

```
for {
        file <- filesHere
        if blabla
    } yield bla

```

- scala 2 vs 3

Nil, Nothing, None(Object), (Any, AnyRef)

```
case object Nil extends List[Nothing] 

/**
 * Dummy class which exist only to satisfy the JVM. It corresponds
 * to `scala.Nothing`. If such type appears in method
 * signatures, it is erased to this one.
 */
sealed abstract class Nothing$ extends Throwable

```

- https://docs.scala-lang.org/overviews/collections-2.13/concrete-immutable-collection-classes.html

-- LazyList usage sample: 
```
def fibFrom(a: Int, b: Int): LazyList[Int] = a #:: fibFrom(b, a + b)

 val fibs = fibFrom(1, 1).take(7)
fibs.toList
```


```
 def lazyFact(f: Int, n: Int) : LazyList[Int] = f #:: lazyFact(f*n, n+1)

 lazyFact(1,1).take(8).toList
 s
```

 Note: To keep reuse it in stack, tailrecurred def must block overriden.So it must be :a. final or private, b.all lower case

 ```
   @scala.annotation.tailrec
  final def doFibo
```

- mixin(scala), cake pattern (self type, so far same thing) : {this: BlaType => }
"mix in": extends .. with ... ...

Note: why we need mixin explained well (compare W. java interface):
https://docs.scala-lang.org/tour/mixin-class-composition.html


### List, then ArraySeq, then Vector
 ArraySeq (2.13) prefered to List for

ArraySeqs store their elements in a private Array. This is a compact representation that supports fast indexed access, but updating or adding one element is linear since it requires creating another array and copying all the original array’s elements.

 cons: ArraySeq is also inefficient in other use cases: for instance, prepending an element is constant for List, but linear for ArraySeq, and, conversely, indexed access is constant for ArraySeq but linear for List.


Vector (2.8)

Every tree node contains up to 32 elements of the vector or contains up to 32 other tree nodes. Vectors with up to 32 elements can be represented in a single node. Vectors with up to 32 * 32 = 1024 elements can be represented with a single indirection. Two hops from the root of the tree to the final element node are sufficient for vectors with up to 215 elements, three hops for vectors with 220

Vectors features:

- Iteration is a little bit slower considering its trcomplications
- Head/Tail, esp tail acces quicker than List or ArraySeq
- Random element access quicker (10000 element its 36 (almost 2^5) times faster, make sense for TRI)
- new element append to head(fast, but not significant)/append to tail(10000 elements is 67times faster)


###

apply vs constructor:

apply is more as factory pattern. Like Array(1,2,3) extends `IterableFactory.apply[A]( a: A*) `




- https://www.baeldung.com/scala/variances
F[+T], F[-T]
In the Scala SDK, the most popular contravariant type constructor is Function1[-T1, +R].

- co/contral-variant, vs java consumer/producer
https://www.cs.rice.edu/~javaplt/nv4/scala-variance/




- monoid, functor, monad
https://blog.knoldus.com/basic-understanding-of-monads-monoids-and-functor/
```
trait Monoid[A] {
    def op(a1: A, a2: A): A
    def zero: A
}

trait Functor[F[_]] {
    def map[A, B](fa: F[A])(f: A => B): F[B]
}

trait Monad[F[_]] {
    def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B]
    def unit[A](A): F[A] 
}
```


 V2 implicit types & practice
- implicit parameter with implicit type instance (mostly method with contextual parameters) : find implicit var in scope: contenxtual provider
- implicit Type conversion (View): implicit def string2Int(str: String): Int = ...
- Exntention methods


  implicit functionality will be replaced V3 with Extension Methods and Givens.



Json lib in scala

https://circe.github.io/circe/


## How to deal with blocking works like CPU heavy duty, IO work, or RMDB's JDBC blocking operations

ExecutionContext, it is basically another name of threadpool

Create a dedicated ExecutionContext for those type of actor or future job confined the blocking impact from overflow to default executionContext, like

```
implicit val ex = myExecutionContext
Future{

}(//no need to declare implicit val here)

```

ref:  https://www.playframework.com/documentation/2.5.x/ThreadPools

# Why no need of ThreadLocal call in Scala

Scala code doesn’t need to use ThreadLocals because it can use implicit parameters to pass context instead

# `break` statement in scala

https://www.baeldung.com/scala/break-statement

























# AKKA http vs spray.io
https://doc.akka.io/docs/akka-http/current/migration-guide/migration-from-spray.html (recommended)

https://www.lightbend.com/blog/lightbend-podcast-when-to-use-play-lagom-or-akka-http


# AKKA open source now is PEKKO

https://github.com/apache/incubator-pekko

# AKKA actor




https://www.baeldung.com/akka-with-spring


https://dzone.com/articles/comparing-akka-streams-kafka-streams-and-spark-str



https://hevodata.com/learn/spark-rest-api/


# AKKA stream

https://doc.akka.io/docs/akka/current/stream/stream-quickstart.html

Source: something with exactly one output stream

Sink: something with exactly one input stream

Flow: A Operator with exactly one input and one output stream 

BidiFlow (Bidirection): something with exactly two input streams and two output streams that conceptually behave like two Flows of opposite

Stream
An active process that involves moving and transforming data.

Graph
A description of a stream processing topology, defining the pathways through which elements shall flow when the stream is running.

RunnableGraph

constructed  by connecting all the source, sink and different operators

Operator

The common name for all building blocks that build up a Graph. Examples of operators are map(), filter(), custom ones extending GraphStages and graph junctions like Merge or Broadcast. Full list of built-in operators: https://doc.akka.io/docs/akka/current/stream/operators/index.html


direction
Graph: a packaged stream processing topology that exposes a certain set of input and output ports, characterized by an object of type Shape.


## AKKA Typed

https://doc.akka.io/docs/akka/current/typed/from-classic.html (vs AKKA classic)

https://blog.genuine.com/2021/03/from-akka-untyped-to-typed-actors/

