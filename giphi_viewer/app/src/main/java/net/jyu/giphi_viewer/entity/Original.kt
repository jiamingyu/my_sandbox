package net.jyu.giphi_viewer.entity

data class Original (

	val height : Int,
	val width : Int,
	val size : Int,
	val url : String,
	val mp4_size : Int,
	val mp4 : String,
	val webp_size : Int,
	val webp : String,
	val frames : Int,
	val hash : String
)