package net.jyu.giphi_viewer.entity

data class Original_mp4 (

	val height : Int,
	val width : Int,
	val mp4_size : Int,
	val mp4 : String
)