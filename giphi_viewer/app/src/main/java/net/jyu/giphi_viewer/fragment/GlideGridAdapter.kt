package net.jyu.giphi_viewer.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import net.jyu.giphi_viewer.databinding.CardBinding
import net.jyu.giphi_viewer.databinding.CardLoadingBinding
import net.jyu.giphi_viewer.entity.Data

@BindingAdapter("previewImg")
fun loadImage(view: ImageView, imageUrl : String) {
    Glide.with(view.context).asGif()
        .load(imageUrl).override(300, 300).centerCrop().into(view)
}

val DIFF_CALL_BACK = object : DiffUtil.ItemCallback<Data>() {
    override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean =
        oldItem.id == oldItem.id
}

class GlideGridAdapter(val context: Context, val fragViewModel: ItemListViewModel) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val differ = AsyncListDiffer<Data>(this, DIFF_CALL_BACK)

    fun submitListWithListDiffer(l: List<Data>) = differ.submitList(l)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            LOAD_HOLDER_VIEW -> {
                LoadingViewHolder(CardLoadingBinding.inflate(LayoutInflater.from(context), parent, false))
            }
            else -> {
                DataViewHolder(CardBinding.inflate(LayoutInflater.from(context), parent, false))
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position < itemCount - 1 -> {
                CARD_HOLDER_VIEW
            }
            else -> {
                LOAD_HOLDER_VIEW
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // Next: detect holder type
        when (holder) {
            is DataViewHolder -> {
                val data = differ.currentList
                if ( position < data.size ) {
                    val singleData = data[position]
                    holder.binding.singleData = singleData

                } else {
                    // logging warn 4 investigation
                }
            }
            is LoadingViewHolder -> {
                holder.bind()
            }
        }
    }

    override fun getItemCount(): Int {
        return (differ.currentList.size?:0) + 1
    }

    companion object {
        const val CARD_HOLDER_VIEW = 10
        const val LOAD_HOLDER_VIEW = 20


    }

    inner class DataViewHolder(val binding: CardBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Data, action: (d: Data) -> Unit ) {
            binding.title.text = data.title
            //define click through
            itemView.setOnClickListener{
                action.invoke(data)
            }
            binding.executePendingBindings()
        }
    }

    inner class LoadingViewHolder(val binding: CardLoadingBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            binding.spinner.visibility = View.VISIBLE
            //TODO: vm.xxProvider.launchLoadData.invoke()
            fragViewModel.getPagenatedData(fragViewModel.pageToLoad)
            binding.executePendingBindings()
        }
    }
}
