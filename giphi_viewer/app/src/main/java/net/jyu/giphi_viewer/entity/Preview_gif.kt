package net.jyu.giphi_viewer.entity

data class Preview_gif (

	val height : Int,
	val width : Int,
	val size : Int,
	val url : String
)