package net.jyu.giphi_viewer.fragment

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.jyu.giphi_viewer.dataservice.GifiRetrofit
import net.jyu.giphi_viewer.entity.Data

class ItemListViewModel : ViewModel() {
    val images = MutableLiveData<List<Data>>().apply{value = mutableListOf<Data>()}
    var pageToLoad = 0

    fun getPagenatedData(pageIndex: Int = 0) {
        viewModelScope.launch(Dispatchers.IO) {
            Log.i("ItemListViewModel", "Trying to load data for page $pageToLoad")
            val resp = GifiRetrofit.apis.getStickers()


            withContext(Dispatchers.Default) {
                resp.data.apply {
                    val d = images.value?.toMutableList()
                    d?.let{
                        it.addAll(this)
                        images.postValue(it)
                        pageToLoad+=1
                    }
                }
            }

        }
    }

}