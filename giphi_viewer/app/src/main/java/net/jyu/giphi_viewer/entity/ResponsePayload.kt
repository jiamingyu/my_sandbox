package net.jyu.giphi_viewer.entity

data class ResponsePayload (

    val data : List<Data>,
    val pagination : Pagination,
//    val meta : Meta
)