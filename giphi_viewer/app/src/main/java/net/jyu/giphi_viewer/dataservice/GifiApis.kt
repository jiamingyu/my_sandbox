package net.jyu.giphi_viewer.dataservice

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import net.jyu.giphi_viewer.entity.ResponsePayload
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

class GifiRetrofit {
    companion object {
        val gson = GsonBuilder().setLenient().create()
        val INSTANCE = Retrofit.Builder().baseUrl("https://api.giphy.com/v1/stickers/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

        val apis = INSTANCE.create(GifiApis::class.java)
    }
}

interface GifiApis {
    @GET("trending?api_key=2cY2gKhynLoFO0G7pfdtOvtAJL0MfR6s&limit=25")
    suspend fun getStickers(@Query("offset") offset: Int = 25): ResponsePayload
}
