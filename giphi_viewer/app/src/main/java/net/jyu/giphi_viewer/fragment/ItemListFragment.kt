package net.jyu.giphi_viewer.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import net.jyu.giphi_viewer.databinding.ItemListBinding

class ItemListFragment : Fragment() {
    lateinit var  viewModel : ItemListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(ItemListViewModel::class.java)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = ItemListBinding.inflate(inflater, container, false)
        val rcylvw = binding.itemList
        rcylvw.apply {
            layoutManager = GridLayoutManager(requireContext(),3)
            adapter = GlideGridAdapter(requireContext(), viewModel)
            viewModel.images.observe(viewLifecycleOwner) { imgs ->
                (adapter as GlideGridAdapter).submitListWithListDiffer(imgs)
            }
        }
        return binding.root
    }
}
