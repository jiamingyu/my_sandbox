
package net.jyu.giphi_viewer.entity

data class Pagination (

	val total_count : Int,
	val count : Int,
	val offset : Int
)