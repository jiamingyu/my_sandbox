package net.jyu.giphi_viewer.entity


data class Original_still (

	val height : Int,
	val width : Int,
	val size : Int,
	val url : String
)