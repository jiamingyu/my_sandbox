package net.jyu.giphi_viewer

import android.content.Context
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.runners.MockitoJUnitRunner

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

@RunWith(MockitoJUnitRunner::class)
class ExampleUnitTest {
    @Mock
    private lateinit var mockContext : Context

    @Test
    fun addition_isCorrect() {
        `when`(mockContext.getString(R.string.app_name)).thenReturn("")

        assertEquals(4, 2 + 2)
    }
}