# Install docker contained cassandra on mac.

## docker prep
run `brew reinstall --cask docker`
then launch docker app


## run cql sh to exec cmd
Note: This is for linux/unix and not working on mac (https://cassandra.apache.org/_/quickstart.html, step 5)
```docker run --rm -it --network cassandra nuvo/docker-cqlsh cqlsh cassandra 9042 --cqlversion='3.4.5' ```

instead run:

```
docker exec -it cassandra /bin/sh
# cqlsh
```

## exit from cqlsh & docker container
```
cqlsh> exit
# cqlsh
```
Ctl + d, to exist from docker container



# Architect review


## CAP 
https://en.wikipedia.org/wiki/CAP_theorem

Cassandra: take "Availability" && "Partition tolerance" as priority, while achieve "eventually Consistent"

## Horizontal scalability by partitioning all data stored in the system using a hash function



## NOT support (comparing to RMDB): Any trying on cross partition (only simple query model, row only identified by partition key but can dynamically variant in columns)

- Cross partition transactions
- Distributed joins
- Foreign keys or referential integrity.




## nomanclatures

Token: A single position on the dynamo style hash ring.

Endpoint: A single physical IP and port on the network.

Host ID: A unique identifier for a single "physical" node, usually present at one gEndpoint and containing one or more gTokens.

Virtual Node (or vnode): A gToken on the hash ring owned by the same physical node, one with the same gHost ID.

# How cluster manage nodes & detect failure
- Gossip


## Storage engine

memtable (in memory)
commit log (disk)
S[orted]S[trig]table (disk, immutable), periodically flushed from memtable


# Data modeling

- sharding vs partition
# Sharding implies the data is spread across multiple computers 
# Partitioning does not. Partitioning is about grouping subsets of data



-
A partition key is generated from the first field of a primary key. 
It is first element of composite primary key (partition key). Below (id1,id2)is the first element while c1,c2 are used for sorting within the partition
```
 PRIMARY KEY ((id1,id2),c1,c2)
```

- Table naming pattern: $records_by_$pkcol , eg: hotels_by_poi


- Cassandra does not have the concept of foreign keys or relational integrity


LWT (light weight transaction) transactions (compare-and-set, conditional update) will impact query && should be minum. IN ANOTHER WORD: POST REST should be most case

-
Beginning with the 3.0 release, Cassandra provides a feature known as materialized views <materialized-views> which allows you to create multiple denormalized views of data based on a base table design

-
in Cassandra you DO NOT start with the data model(which happened in relational DB); you start with the query model.
```
create materialized view xxx as select ...
```

- 
```
CREATE TABLE hotel.hotels_by_poi (
  poi_name text,
  hotel_id text,
  name text,
  phone text,
  address frozen<address>,
  PRIMARY KEY ((poi_name), hotel_id) )
  WITH comment = ‘Q1. Find hotels near given poi’
  AND CLUSTERING ORDER BY (hotel_id ASC) ;
```

- Partition limit
Cassandra’s hard limit is 2 billion cells per partition (Not include pk and static col cells)

- When to use cassandra
https://blog.softwaremill.com/7-mistakes-when-using-apache-cassandra-51d2cf6df519


- timeseries DB (TSDB) discussion
https://www.grin.com/document/299975#:~:text=Capabilities%20of%20NoSQL%20databases%20match,schemas%20and%20specific%20query%20functionalities.

- To break up large partition: A. Extra col as primary key. B.bucketing (introduce a var in partition key for later partition control)
```
CREATE TABLE tweet_stream (
    account text,
    day text,
    bucket int,
    ts timeuuid,
    message text,
    primary key((account, day, bucket), ts)
) WITH CLUSTERING ORDER BY (ts DESC) 
         AND COMPACTION = {'class': 'TimeWindowCompactionStrategy', 
                       'compaction_windo ...
```

- partition, clustering, composite key:
https://www.baeldung.com/cassandra-keys
eg:
PRIMARY KEY ((a, b), c, d): The composite partition key is (a, b), the composite clustering key is (c, d).  Clustering in CQL just define order

