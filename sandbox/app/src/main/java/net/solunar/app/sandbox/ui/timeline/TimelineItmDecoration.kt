package net.solunar.app.sandbox.ui.timeline

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import net.solunar.app.sandbox.R
import net.solunar.app.sandbox.ui.timeline.util.Event


class TimelineItmDecoration(private val context: Context, val laneIdx : Int)
    : RecyclerView.ItemDecoration() {
    private val linePaint: Paint = Paint().apply{color = context.resources.getColor(R.color.colorLinePaint, context.theme); strokeWidth = 16F }

    private val zoomInPaint: Paint = Paint().apply { color = context.resources.getColor(R.color.colorTimelineFontPaint, context.theme); textSize = 40F }

    private val zoomOutPaint: Paint = Paint().apply { color = context.resources.getColor(R.color.colorTimelineFontPaint, context.theme); textSize = 20F}

    //ItemView top left offset
    private val itemView_leftinterval = 200
    private val itemView_topintervarl = 50

    //Radius of pivot point
    private val circle_radius = 10f

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        // customize
        outRect.set(itemView_leftinterval, itemView_topintervarl, 0, 0)
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)

        parent.children.forEachIndexed { idx, cv ->

            val centerx : Float = (cv.left - itemView_leftinterval/3).toFloat()
            val centery : Float = (cv.top - itemView_topintervarl + (itemView_topintervarl + cv.height/2)).toFloat()


            /** top half axis */
            // upper
            val upLine_up_x = centerx
            val upLine_up_y: Float = (cv.top - itemView_topintervarl).toFloat()

            // lower
            val upLine_down_x = centerx
            val upLine_down_y = centery - circle_radius
            c.drawLine(upLine_up_x, upLine_up_y, upLine_down_x, upLine_down_y, linePaint)

            /** bottom half axis */
            // upper endpoint
            val bottomLine_up_x = centerx
            val bottom_up_y = centery + circle_radius

            // lower endpoint
            val bottomLine_bottom_x = centerx
            val bottomLine_bottom_y = cv.bottom.toFloat()
            c.drawLine(bottomLine_up_x, bottom_up_y, bottomLine_bottom_x, bottomLine_bottom_y, linePaint)

            /** Text */
            val textX = (cv.left - itemView_leftinterval * 5/6).toFloat()
            val textY = upLine_down_y

            val evnt = TimeLineViewModel.requestLanesValue()[laneIdx][idx]

            if (idx == TimeLineViewModel.selectedPosition) {
                c.drawText(evnt.thinDateRange(), textX, textY, zoomInPaint)
            } else {
                c.drawText(evnt.thinDateRange(), textX, textY, zoomOutPaint)

            }
        } // End of forEach
    }


}

fun Event.thinDateRange() : String {
    val min = startDate
    val max = endDate

    return StringBuilder().append(min.year-100).append(".").append(min.month).append(".").append(min.date)
        .append("-").append(max.month).append(".").append(max.date).toString()
}