package net.solunar.app.sandbox.ui.timeline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import net.solunar.app.sandbox.databinding.FragmentEditEventBinding

private const val ARG_PARAM1 = "lane_id"
private const val ARG_PARAM2 = "event_id"


class EventEditFragment : Fragment() {
    private var laneId  = -1
    private var eventId = -1
    private var position = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            laneId = it.getInt(ARG_PARAM1)
            eventId = it.getInt(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bind = FragmentEditEventBinding.inflate(inflater, container, false)

        val dataSet = TimeLineViewModel.requestLanesValue() as MutableList
        dataSet[laneId]?.let{es ->
            val p = es.forEachIndexed { index, event ->
                if (event.id == eventId) {
                    position = index
                }
            }
        }

        bind.eventCase = dataSet[laneId][position]

        bind.update.setOnClickListener {
            dataSet[laneId][position].name = bind.eventName.text.toString().trim()
            TimeLineViewModel.lanes.postValue(dataSet)
            findNavController().navigate(EventEditFragmentDirections.actionEditEventToNavigationHome())
        }
        bind.cancelUpdate.setOnClickListener {
            findNavController().navigate(EventEditFragmentDirections.actionEditEventToNavigationHome())
        }
        return bind.root
    }

}