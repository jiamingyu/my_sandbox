package net.solunar.app.sandbox.ui.timeline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ScaleGestureDetector
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import net.solunar.app.sandbox.databinding.FragmentTimelineBinding

class TimeLineFragment : Fragment() {

    private lateinit var timeLineViewModel: TimeLineViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        timeLineViewModel =
            ViewModelProvider(this).get(TimeLineViewModel::class.java)
    }

    private var scaleGestureDetector: ScaleGestureDetector? = null
    private val TAG: String = TimeLineFragment::class.java.getSimpleName()

    private var scaleFactor = 1f
    private val minScale = 1.0f
    private val maxScale = 3.0f

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = FragmentTimelineBinding.inflate(LayoutInflater.from(context), container, false)

        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                binding.tabLayout.getTabAt(position)?.let{
                    it.select()
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        binding.viewPager.adapter = LaneAdapter(requireContext(), parentFragmentManager)

        TimeLineViewModel.lanes.value?.forEachIndexed { idx, ln ->
            val tablayout = binding.tabLayout
            tablayout.addTab(tablayout.newTab().setText("lane ${idx+1}"))
            tablayout.isSmoothScrollingEnabled = true
        }

        binding.tabLayout.addOnTabSelectedListener(object:
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.apply {
                    binding.viewPager.currentItem = position
                }

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })


        return binding.root
    }
}
