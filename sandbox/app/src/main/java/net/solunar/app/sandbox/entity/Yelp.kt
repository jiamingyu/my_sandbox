package net.solunar.app.sandbox.entity


data class YelpSearchResponse (

    val businesses : List<Business>
)

data class Business (

    val id : String,
    val name : String,
    val image_url : String,
    val categories : List<Categories>,
    val rating : Double,
    val price : String,
    val location : Location,
    val distance : Double
)


data class Location (

    val address1 : String,
    val address2 : String,
    val address3 : String,
    val city : String,
    val zip_code : Int,
    val country : String,
    val state : String,
    val display_address : List<String>
)

data class Categories (

    val alias : String,
    val title : String
)
