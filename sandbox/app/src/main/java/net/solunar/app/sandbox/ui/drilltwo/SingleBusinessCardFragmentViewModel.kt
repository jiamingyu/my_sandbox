package net.solunar.app.sandbox.ui.drilltwo

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SingleBusinessCardFragmentViewModel() : ViewModel() {
    val image = MutableLiveData<Bitmap>()

}