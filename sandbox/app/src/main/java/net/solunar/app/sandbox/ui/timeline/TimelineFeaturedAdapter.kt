package net.solunar.app.sandbox.ui.timeline

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_layout.view.*
import net.solunar.app.sandbox.databinding.ItemLayoutBinding
import net.solunar.app.sandbox.ui.timeline.util.Event
import net.solunar.app.sandbox.ui.timeline.util.displayStartTimeThin


class TimelineFeaturedAdapter(val laneIndex : Int, val navFun : (Int, Int) -> Unit) : RecyclerView.Adapter<TimelineFeaturedAdapter.FeaturedViewHolder>() {

    class FeaturedViewHolder(val bind: ItemLayoutBinding) : RecyclerView.ViewHolder(bind.root) {
        fun toBind(event: Event) {


            bind.itemText.text = event.name
            bind.itemTitle.text = event.id.toString()
            bind.itemDateRange.text = event.displayStartTimeThin()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedViewHolder {
        val binding = ItemLayoutBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )

        return FeaturedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FeaturedViewHolder, position: Int) {
        val e = TimeLineViewModel.requestLanesValue()[laneIndex][position]



        holder.itemView.isLongClickable = true

        e?.let {
            holder.toBind(it)
            holder.itemView.setOnClickListener {
                TimeLineViewModel.selectedPosition = position
                this.notifyDataSetChanged()
            }

            holder.itemView.edit_event.setOnClickListener {
                navFun.invoke(laneIndex, e.id)
            }

        }
    }

    override fun getItemCount(): Int = TimeLineViewModel.requestLanesValue()[laneIndex].size

}