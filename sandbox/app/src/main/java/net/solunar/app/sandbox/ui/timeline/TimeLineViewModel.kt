package net.solunar.app.sandbox.ui.timeline

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.solunar.app.sandbox.ui.timeline.util.Event
import net.solunar.app.sandbox.ui.timeline.util.assignLanes
import net.solunar.app.sandbox.ui.timeline.util.timelineItems

class TimeLineViewModel : ViewModel() {
    companion object {
        var selectedPosition = -1
        val lanes : MutableLiveData<List<List<Event>>> = MutableLiveData(assignLanes(timelineItems))
        fun requestLanesValue() : List<List<Event>> {
            return lanes.value?:listOf(listOf<Event>())
        }
    }
}