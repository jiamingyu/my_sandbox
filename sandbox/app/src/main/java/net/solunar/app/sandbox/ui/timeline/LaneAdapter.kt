package net.solunar.app.sandbox.ui.timeline

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class LaneAdapter(val context: Context, val fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int {
        return     TimeLineViewModel.requestLanesValue().count()

    }
    override fun getItem(position: Int): Fragment {
        return LaneFragment(position)
    }
}