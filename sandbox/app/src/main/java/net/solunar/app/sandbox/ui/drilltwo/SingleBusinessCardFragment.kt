package net.solunar.app.sandbox.ui.drilltwo

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.CoroutineExceptionHandler
import net.solunar.app.sandbox.databinding.FragmentSingleYelpBusinessBinding
import net.solunar.app.sandbox.dsl.guaranteedToastShow
import net.solunar.app.sandbox.dsl.ioSafeJob
import net.solunar.app.sandbox.entity.Business
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class SingleBusinessCardFragment(val business: Business?) : Fragment() {
    val viewModel : SingleBusinessCardFragmentViewModel by viewModels()

    override fun onResume() {
        super.onResume()

        business?.let{
            lifecycleScope.launchWhenResumed {
                ioSafeJob (
                    CoroutineExceptionHandler { _, e ->
                        lifecycleScope.guaranteedToastShow("Get Exception when tyring to download image", context)
                    }
                ){
                    val url = URL(it.image_url)
                    val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
                    connection.doInput = true
                    connection.connect()
                    val input: InputStream = connection.getInputStream()

                    viewModel.image.postValue(BitmapFactory.decodeStream(input))
                }
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentSingleYelpBusinessBinding.inflate(inflater, container, false)
        viewModel.image.observe(viewLifecycleOwner, Observer {
            binding.imageOfBusiness.setImageBitmap(it)
            // Or you can use below to dynamically set
            // binding.imageOfBusiness.setImageBitmap(Bitmap.createScaledBitmap(it, R.dimen.post_image_height, 120, false))
        })
        business?.let {
            binding.business = it

        }

        return binding.root
    }

}
