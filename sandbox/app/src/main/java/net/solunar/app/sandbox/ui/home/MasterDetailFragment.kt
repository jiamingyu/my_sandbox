package net.solunar.app.sandbox.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.solunar.app.sandbox.R
import net.solunar.app.sandbox.databinding.ItemListBinding

class MasterDetailFragment : Fragment() {
    lateinit var binding: ItemListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = ItemListBinding.inflate(inflater, container, false)

//        binding.itemListRecyView.apply {
            binding.itemListRecyView.layoutManager = LinearLayoutManager(context)
            binding.itemListRecyView.adapter = SimpleItemRecyclerViewAdapter(DummyContent.ITEMS, this)
//        }

        return binding.root
    }
}


class SimpleItemRecyclerViewAdapter(
    private val values: List<DummyContent.DummyItem>, private val parentFragment : Fragment
) :
    RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as DummyContent.DummyItem
            findNavController(parentFragment).navigate(MasterDetailFragmentDirections.master2Detail(
                M2DArgs(item.id)
            ))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.id
        holder.contentView.text = item.content

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idView: TextView = view.findViewById(R.id.id_text)
        val contentView: TextView = view.findViewById(R.id.content)
    }
}
