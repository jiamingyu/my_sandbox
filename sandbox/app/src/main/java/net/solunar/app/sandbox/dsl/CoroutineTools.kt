package net.solunar.app.sandbox.dsl

import android.util.Log
import android.widget.Toast
import kotlinx.coroutines.*

val defaultCEH = CoroutineExceptionHandler{ _, e ->
    Log.e("Default CEH Handler","Default way handling exception in Coroutine Exception Handler: ${e.toString()}")
}

fun CoroutineScope.ioSafeJob(
    ceh: CoroutineExceptionHandler = defaultCEH,
    block: suspend () -> Unit): Unit {
    safeJob(Dispatchers.IO, ceh, block)
}

fun CoroutineScope.computeSafeJob(
    ceh: CoroutineExceptionHandler = defaultCEH,
    block: suspend () -> Unit): Unit {
    safeJob(Dispatchers.Default, ceh, block)
}

fun <T> CoroutineScope.ioSafeDeferred(
    ceh: CoroutineExceptionHandler = defaultCEH,
    block: suspend () -> T
): Deferred<T> {
    return async(Dispatchers.IO +SupervisorJob() + ceh) {
        block.invoke()
    }
}

fun CoroutineScope.safeJob(
    dispatcher: CoroutineDispatcher,
    ceh: CoroutineExceptionHandler = defaultCEH,
    block: suspend () -> Unit): Unit {
    if (dispatcher == Dispatchers.Main) {
        return
    }
    launch( dispatcher +SupervisorJob() + ceh) {
        block.invoke()
    }
}

fun CoroutineScope.guaranteedToastShow(msg: String, ctx: android.content.Context?) {
    ctx?.let{
        launch( Dispatchers.Main) {
            Toast.makeText(it, msg, Toast.LENGTH_LONG).show()
        }
    }
}

fun CoroutineScope.mainJob(block: suspend ()->Unit) {

}
