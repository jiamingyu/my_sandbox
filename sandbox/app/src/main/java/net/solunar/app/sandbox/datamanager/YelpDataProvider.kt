package net.solunar.app.sandbox.datamanager

import net.solunar.app.sandbox.entity.Business
import net.solunar.app.sandbox.retrofit.YelpRetrofit
import net.solunar.app.sandbox.ui.drilltwo.VP2YelpFragmentViewModel

class YelpDataProvider(val viewModel: VP2YelpFragmentViewModel) {

    suspend fun loadMore(): List<Business> {
        // api call
        val businesses = viewModel.searchParameter.value?.run {
            val x = YelpRetrofit.apis.searchByLatLong(
                latitude, longitude, offset, limit
            )
            x.businesses
        }
        val newSearchParams = viewModel.searchParameter.value?.apply {
            offset += limit
        }
        viewModel.searchParameter.postValue(newSearchParams)

        return businesses?:listOf()

    }

}

