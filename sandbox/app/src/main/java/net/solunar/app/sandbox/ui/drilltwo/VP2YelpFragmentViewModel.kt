package net.solunar.app.sandbox.ui.drilltwo

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import net.solunar.app.sandbox.datamanager.YelpDataProvider
import net.solunar.app.sandbox.entity.Business
import net.solunar.app.sandbox.retrofit.YelpSearchParams

class VP2YelpFragmentViewModel : ViewModel() {
    val searchParameter = MutableLiveData<YelpSearchParams>().apply {value = YelpSearchParams() }

    val resturantList : MutableLiveData<MutableList<Business>> = MutableLiveData<MutableList<Business>>().apply {
        value = mutableListOf<Business>()
    }

    val id2Image = MutableLiveData<MutableMap<String, Bitmap>>().apply {
        value = mutableMapOf<String, Bitmap>()
    }

    val dataProvider = YelpDataProvider(this)

    @ExperimentalCoroutinesApi
    suspend fun loadMoreAndUpdateData(): Boolean {
        val moreData = dataProvider.loadMore()
        val existingData = resturantList.value?: mutableListOf<Business>()
        existingData.addAll(moreData.toMutableList())
        resturantList.postValue(existingData)
        return true
    }

}
