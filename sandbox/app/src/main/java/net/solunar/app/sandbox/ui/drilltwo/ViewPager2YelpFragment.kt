package net.solunar.app.sandbox.ui.drilltwo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import net.solunar.app.sandbox.databinding.FragmentYelpViewpager2Binding

class ViewPager2YelpFragment : Fragment(), CoroutineScope by MainScope() {

    lateinit var vP2YelpFragmentViewModel: VP2YelpFragmentViewModel
    private lateinit var binding : FragmentYelpViewpager2Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        vP2YelpFragmentViewModel =
            ViewModelProvider(this).get(VP2YelpFragmentViewModel::class.java)
        binding = FragmentYelpViewpager2Binding.inflate(inflater, container, false)

        // configure viewpager2
        activity?.let {act ->
            binding.restaurantList.adapter = YelpFragmentStateAdapter(this, act)
        }

        vP2YelpFragmentViewModel.resturantList.observe(viewLifecycleOwner) {
            binding.restaurantList.adapter?.notifyDataSetChanged()
        }

        lifecycleScope.launch {
            // retrofit now handle run api call on IO dispatcher itself
            vP2YelpFragmentViewModel.loadMoreAndUpdateData()
        }

        // configure 2 float action buttons

        // Configure location service workmanager

        return binding.root
    }

    fun loadMoreAndUpdateBusiness() {
        launch {
            // retrofit now handle run api call on IO dispatcher itself
            vP2YelpFragmentViewModel.loadMoreAndUpdateData()
            // This is mainScope so guranatee notify happening on main thread
            binding.restaurantList.adapter?.notifyDataSetChanged()
        }
    }


}