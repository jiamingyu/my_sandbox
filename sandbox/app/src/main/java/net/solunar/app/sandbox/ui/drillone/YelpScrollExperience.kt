package net.solunar.app.sandbox.ui.drillone

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.coroutines.launch
import net.solunar.app.sandbox.databinding.CardSingleBusinessBinding
import net.solunar.app.sandbox.databinding.FragmentYelpScrollBinding
import net.solunar.app.sandbox.databinding.LoadingCardBinding
import net.solunar.app.sandbox.entity.Business
import net.solunar.app.sandbox.ui.drilltwo.VP2YelpFragmentViewModel

class YelpScrollExperience : Fragment() {

    private lateinit var yelpScrollViewModel: VP2YelpFragmentViewModel
    lateinit var binding : FragmentYelpScrollBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        yelpScrollViewModel =
            ViewModelProvider(this).get(VP2YelpFragmentViewModel::class.java)
        binding = FragmentYelpScrollBinding.inflate(inflater, container, false)

        binding.businessRecyclerView.apply {
            adapter = YelpRecyclerViewAdapter()
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            loadMoreAndUpdateBusiness()
        }
    }

    suspend fun loadMoreAndUpdateBusiness() {
        // retrofit now handle run api call on IO dispatcher itself
        yelpScrollViewModel.loadMoreAndUpdateData()
        // This is mainScope so guranatee notify happening on main thread
        binding.businessRecyclerView.adapter?.notifyDataSetChanged()
    }

//    open class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view)


    companion object {
        const val ITM_BUS = 100
        const val ITM_LOAD = 200
    }

    inner class YelpRecyclerViewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        init {
            val x = 2
        }

        inner class BusinessViewHolder(val cardBinding: CardSingleBusinessBinding) : RecyclerView.ViewHolder(cardBinding.root) {
            fun bind(business: Business) {
                cardBinding.singleBusiness =business
                cardBinding.executePendingBindings()
            }
        }

        inner class LoadingViewHolder(val cardBinding: LoadingCardBinding) : RecyclerView.ViewHolder(cardBinding.root){
            fun bind() {
                lifecycleScope.launchWhenCreated {
                    loadMoreAndUpdateBusiness()
                }
                cardBinding.executePendingBindings()
            }
        }


        override fun getItemViewType(position: Int): Int =
            when (position) {
                yelpScrollViewModel.resturantList.value!!.size -> ITM_LOAD
                else -> ITM_BUS
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val inflater = LayoutInflater.from(context)
           return when (viewType) {
               ITM_LOAD -> {
                   LoadingViewHolder( LoadingCardBinding.inflate(inflater, parent, false) )
               }
               else -> {
                    BusinessViewHolder( CardSingleBusinessBinding.inflate(inflater, parent, false) )
                }
            }

        }

        override fun getItemCount(): Int {
            return yelpScrollViewModel.resturantList.value!!.size + 1
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            when (holder) {
                is BusinessViewHolder -> {
                    val restaurant :  Business? = yelpScrollViewModel.resturantList.value?.get(position)
                    restaurant?.let {
                        holder.bind(it)
                        it.image_url?.let { url ->
                            Glide.with(requireContext()).asBitmap().load(url).override(400, 400)
                                .centerCrop() //scales the image so that it fills the requested bounds and then crops the extra.
                                .into(holder.cardBinding.imageBusiness)

                        }}
                }
                else -> (holder as LoadingViewHolder).bind()
            }
        }
    }

}

