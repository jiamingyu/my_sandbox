package net.solunar.app.sandbox.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import net.solunar.app.sandbox.databinding.ItemDetailBinding
import java.io.Serializable

class ItemDetailFragment : Fragment() {

    lateinit var itemDetailBinding : ItemDetailBinding

    val args: ItemDetailFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        itemDetailBinding = ItemDetailBinding.inflate(inflater, container, false)
        val itemId = args.master2DetailArgs.itemId
        itemId?.let {
            DummyContent.ITEM_MAP[it]?.let { itm ->
                itemDetailBinding.itemDetail.text = itm.details
            }
        }

        return itemDetailBinding.root
    }

}

data class M2DArgs(val itemId: String?) : Serializable
