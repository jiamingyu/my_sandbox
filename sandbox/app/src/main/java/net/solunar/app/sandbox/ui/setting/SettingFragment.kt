package net.solunar.app.sandbox.ui.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import net.solunar.app.sandbox.databinding.FragmentSettingBinding

class SettingFragment : Fragment() {

    lateinit var fragmentSettingBinding : FragmentSettingBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        fragmentSettingBinding = FragmentSettingBinding.inflate(inflater, container, false)

        fragmentSettingBinding.viewPager.adapter =
            getFragmentManager()?.let { SectionsPagerAdapter(requireContext(), it) }

        val tabs = fragmentSettingBinding.tabs
        tabs.setupWithViewPager(fragmentSettingBinding.viewPager)

        return fragmentSettingBinding.root
    }
}