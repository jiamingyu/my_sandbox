package net.solunar.app.sandbox.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import net.solunar.app.sandbox.databinding.FragmentUserProfileBinding

class UserProfileFragment : Fragment() {

    lateinit var fragmentUserProfileBinding : FragmentUserProfileBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        fragmentUserProfileBinding = FragmentUserProfileBinding.inflate(inflater, container, false)
        return fragmentUserProfileBinding.root
    }
}