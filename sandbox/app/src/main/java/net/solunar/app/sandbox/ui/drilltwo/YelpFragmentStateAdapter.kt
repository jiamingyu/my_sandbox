package net.solunar.app.sandbox.ui.drilltwo

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class YelpFragmentStateAdapter(val parentFragment: ViewPager2YelpFragment,
                               fa: FragmentActivity):  FragmentStateAdapter(fa) {

    override fun getItemCount(): Int = parentFragment.vP2YelpFragmentViewModel.resturantList.value?.size?:0

    /**
     * FrgmentStateAdapter version, @code getItemCount should only be 0 - backed collection size.
     * This impacted the implementation of this methond as following:
     *
     * a. Condition check should only based on position & collection size
     * b. position should always be under collection size -1, and no need to check more than that.
     */
    override fun createFragment(position: Int): Fragment {
        val resturantList = parentFragment.vP2YelpFragmentViewModel.resturantList.value
        return when (position)  {
            resturantList?.size?.minus(1) -> {
                SingleBusinessCardFragment(resturantList?.get(position))
            }
            else -> SingleBusinessCardFragment(resturantList?.get(position))
        }
    }
}
