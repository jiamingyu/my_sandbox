package net.solunar.app.sandbox.retrofit

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import net.solunar.app.sandbox.entity.YelpSearchResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class YelpRetrofit {
    companion object {
        val gson = GsonBuilder().setLenient().create()
        val INSTANCE = Retrofit.Builder().baseUrl("https://api.yelp.com/v3/businesses/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

        val apis = INSTANCE.create(YelpApis::class.java)
    }
}

interface YelpApis {
    @GET("search?")
    @Headers("Authorization: Bearer itoMaM6DJBtqD54BHSZQY9WdWR5xI_CnpZdxa3SG5i7N0M37VK1HklDDF4ifYh8SI-P2kI_mRj5KRSF4_FhTUAkEw322L8L8RY6bF1UB8jFx3TOR0-wW6Tk0KftNXXYx")
    suspend fun  searchByLatLong(@Query("latitude") latitude: String, @Query("longitude") longitude: String, @Query("offset") offset: Int, @Query("limit") limit: Int): YelpSearchResponse

}

data class YelpSearchParams (var latitude: String = "37.3872324410459", var longitude: String = "-121.88542235463", var offset: Int = DEFAULT_OFFSET, var limit: Int = DEFAULT_LIMIT ){
    companion object {
        const val DEFAULT_LIMIT = 20
        const val DEFAULT_OFFSET = 0
    }
}

suspend fun CoroutineScope.getBitmapFromURL(src: String?): Bitmap? = async {
    try {
        val url = URL(src)
        val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
        connection.doInput = true
        connection.connect()
        val input: InputStream = connection.getInputStream()
        BitmapFactory.decodeStream(input)
    } catch (e: IOException) {
        e.printStackTrace()
        null
    }
}.await()
