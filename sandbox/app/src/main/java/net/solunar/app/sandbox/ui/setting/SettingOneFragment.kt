package net.solunar.app.sandbox.ui.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import net.solunar.app.sandbox.R

class SettingOneFragment : Fragment() {


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_setting_one_tab, container, false)
        val textView: TextView = root.findViewById(R.id.text_slideshow)
        textView.text = "Setting one tab"
        return root
    }
}