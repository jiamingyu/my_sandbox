package net.solunar.app.sandbox.ui.timeline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ScaleGestureDetector
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.solunar.app.sandbox.databinding.LaneFragmentBinding
import java.lang.Float

class LaneFragment(val tabIdx : Int) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = LaneFragmentBinding.inflate(LayoutInflater.from(context), container, false)

        TimeLineViewModel.lanes.observe (viewLifecycleOwner){
            binding.myRecyclerView.adapter?.apply{
                    notifyDataSetChanged()
                }
        }

        val scaleGestureDetector2 = object: ScaleGestureDetector(context, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {

            override fun onScale(detector: ScaleGestureDetector): Boolean {
                var scaleFactor = detector.scaleFactor
                if (Float.isNaN(scaleFactor) || Float.isInfinite(scaleFactor)) return false

                scaleFactor = Math.max(1.0f, Math.min(scaleFactor, 3.0f))

                binding.myRecyclerView.scaleX = scaleFactor
                binding.myRecyclerView.scaleY = scaleFactor

                return true
            }
            override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
                return true
            }
            override fun onScaleEnd(detector: ScaleGestureDetector) {}
        }){}


        val timelineItmDecoration = TimelineItmDecoration(this@LaneFragment.requireContext(), tabIdx)

        val navCurryFun : (Int, Int) -> Unit = { lnId, eId ->
            findNavController().navigate(TimeLineFragmentDirections.actionNavigationHomeToEditEvent(laneId = lnId, eventId = eId ))
        }
        val adpt = TimelineFeaturedAdapter(tabIdx, navCurryFun)

        val itemOperationHandler = object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val sid = (viewHolder as TimelineFeaturedAdapter.FeaturedViewHolder).bind.itemTitle.text // event id

                val lanesValue = TimeLineViewModel.requestLanesValue().toMutableList()
                val events2 = lanesValue[tabIdx].filter{
                    !it.id.toString().equals(sid)
                }
                lanesValue[tabIdx] = events2

                TimeLineViewModel.lanes.postValue(lanesValue)
                binding.myRecyclerView.adapter?.apply{notifyDataSetChanged()}

                Toast. makeText(requireContext(),"This event is removed",Toast. LENGTH_SHORT).show();
            }

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                val startPosition = viewHolder.adapterPosition
                val targetPosition = target.adapterPosition

                val lanesValue = TimeLineViewModel.requestLanesValue().toMutableList()
                val eventsOfLane = lanesValue[tabIdx]

                val eventsReOrdered = eventsOfLane.toMutableList()
                // swap positions, && exchange start / end date
                eventsReOrdered[targetPosition] = eventsOfLane[startPosition].copy(
                    startDate = eventsOfLane[targetPosition].startDate,
                    endDate = eventsOfLane[targetPosition].endDate
                )
                eventsReOrdered[startPosition] = eventsOfLane[targetPosition].copy(
                    startDate = eventsOfLane[startPosition].startDate,
                    endDate = eventsOfLane[startPosition].endDate
                )
                lanesValue[tabIdx] = eventsReOrdered
                TimeLineViewModel.lanes.postValue(lanesValue)
                return true
            }
        }


        binding.myRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@LaneFragment.requireContext())
            adapter = adpt

            addItemDecoration(timelineItmDecoration)
            scaleGestureDetector = scaleGestureDetector2

            val itemTouchHelper = ItemTouchHelper(itemOperationHandler)
            itemTouchHelper.attachToRecyclerView(this)

        }
        return binding.root
    }
}